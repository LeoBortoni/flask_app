## Introduction

This application deploy a simple REST API through
Flask, implementing the POST method for the following routes:

    /sort
    /vowel_count

The POST verb must be provided with a content type application/json.

The '/sort' route expects a content as dictionary with the keys 'words' and 'order'. The key 'words' expects a list of strings. The key 'order' expects a string either 'desc' or 'asc'. An example is depicted bellow:

    {"words": ["batman", "robin", "coringa"], "order": "asc"}

The method returns a list of strings ordered according to the 'order' provided.

The route '/vowel_count' expects a dictionary with the key 'words' assigning to a list of strings.

    {"words": ["batman", "robin", "coringa"]}

The method returns a dictionary with key values as being the strings of the lists, assigning to the number of vowels of each string.

This application is deployed locally in an Unix distribution through nginx and gunicorn. The following steps present instructions to deploy it.

## Deploying

### Requirements

* python 3.7.6
* nginx 1.18.0
* Ubunto 20.04

Notes: The deployment was tested with the conditions listed above. The application might work with other versions of Python and nginx, but were not tested. The same for the ubunto version.

### Installing

First, it is necessary to install Python 3.7.6. It may be done following the steps here:

> https://www.python.org/downloads/release/python-376/

Afterwards, if you wish so, you may install a virtual environment and use it to keep your python modules specific for this application. It may be done following the steps in:

> https://docs.python.org/3.7/library/venv.html

If you are using a virtual environment, don't forget to activate it in order to proceed with the following steps.

Go to the folder where the app.py file is located, and Install the required python packages:

```bash
pip3 install -r requirements.txt
```

Note: If you are using a virtual environment, type just 'pip' instead of 'pip3'.

Subsequently, install nginx:

```bash
sudo apt-get update
sudo apt-get install nginx
```

And create a nginx config file for this application:

```bash
sudo nano /etc/nginx/sites-enabled/flask_app
```

Next copy and paste the following content to the file opened with 'nano':

``` bash
server {
        listen 80;
        location / {
                proxy_pass http://127.0.0.1:8000;
                proxy_set_header Host $host;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

        }
}
```

Save and close the 'nano' editor, and reload nginx module:

```bash
sudo nginx -s reload
```

Now we need to install 'gunicorn'. If you are using a virtual environment, activate it.

``` bash
pip3 install gunicorn
```

The 'gunicorn' will be set to the port 8000 as default, and will act as a bridge between our flask application and the http incoming at the port 80, proxyed by nginx to gunicorn at port 8000.

Inside the folder where app.py is located, type:

``` bash
gunicorn -w 4 app:app
```

The '-w' flag declares four workers for the process. The 'app:app'  says to gunicorn to look after the variable 'app' inside the python application 'app.py'.

The application should be running and should see something like that:

![image-20220529230752228](/home/leonardo/.config/Typora/typora-user-images/image-20220529230752228.png)

If you want to set the gunicorn in background, it is possible to set it as a daemon, just providing the flag '--daemon' after the 'app:app'.

## Testing the deployment

In order to test the deployment, you need to download a tool known as Postman.

> https://www.postman.com/downloads/

Open Postman, set a new collection, and fill the fields as following:

![image-20220529231257901](/home/leonardo/.config/Typora/typora-user-images/image-20220529231257901.png)

Don't forget to config the content type, in the field 'Header':

![image-20220529231419224](/home/leonardo/.config/Typora/typora-user-images/image-20220529231419224.png)

Note that we are trying to send a POST request to the address 127.0.0.1/vowel_count. Were it not deployed, there would be a need to provide the port where the flask application is located. But gunicorn is taking care of the interface for us. Click SEND.

![image-20220529231647266](/home/leonardo/.config/Typora/typora-user-images/image-20220529231647266.png)

Let's test the route '/sort':

![image-20220529231722745](/home/leonardo/.config/Typora/typora-user-images/image-20220529231722745.png)

Again, set the header to the appropriated content type. Click SEND:

![image-20220529231825538](/home/leonardo/.config/Typora/typora-user-images/image-20220529231825538.png)

![image-20220529231914566](/home/leonardo/.config/Typora/typora-user-images/image-20220529231914566.png)

That is it.
