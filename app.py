from flask import Flask, request
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

@app.errorhandler(404)
def page_not_found(e):
    """
    This function overwrites the standard template rendered
    by flask 404 error, yielding a dictionary with a error message.
    """
    return {"message":"Route unexistent."}, 404

class VowelCounter(Resource):
    """
    This class provides the API for the functionality of the POST
    requirement to the access point '/vowel_count'.

    This API provides the request just to the POST verb. Other verbs
    are not allowed.
    """

    def post(self):
        """
        This function will look after a request containing a dictionary with the
        key 'words' assigning to a list of strings, following the example:
            
            {"words": ["batman", "robin", "coringa"]}
        
        A bad request error will be returned if the payload is not set appropriated.

        return: It returns a dictionary where the keys are the strings given in the list
        assigned to the key 'words' in the payload, where each string is assigned to a integer
        which represents the amount of vowels of the string.
        """
        if request.content_type != "application/json":
            return {"message":"Content type must be application/json"}, 415

        data = request.get_json()

        if isinstance(data,dict):

            if "words" in data:

                if isinstance(data["words"],list) and (len(data["words"])>0):
                    response = {}
                    vowels = "aeiou"

                    for item in data["words"]:
                        if not isinstance(item,str):
                            return {"message":"The content of the list provided must be a string."}, 400
                        word = item.lower()
                        response[item] = sum([1 for i in word if i in vowels])

                    return response, 200
  
        return {"message":"The content provided must be a dictionary with the key 'words' assigning to a list of strings (non empty). For for instance: {'words':['aaa','bbb']}"}, 400

    def get(self):
        return {"message":"Route allows just POST method"}, 400
    
    def put(self):
        return {"message":"Route allows just POST method"}, 400

    def delete(self):
        return {"message":"Route allows just POST method"}, 400

class Sorter(Resource):
    """
    This class provides the API for the functionality of the POST
    requirement to the access point '/sort'.

    This API provides the request just to the POST verb. Other verbs
    are not allowed.
    """

    def post(self):
        """
        This function will look after a request containing a dictionary with the
        keys 'words'and 'order', assigned to a list of strings and a string, respectivelly, following the example:
            
            {"words": ["batman", "robin", "coringa"], "order": "asc"}
        
        A bad request error will be returned if the payload is not set appropriated. 
        Note: The 'order' value must be either 'desc' or 'asc'.

        return: It returns a list with the strings given to the list assigned to 'words'
        sorted according to the 'order' set.
        """
        if request.content_type != "application/json":
            return {"message":"Content type must be application/json"}, 415

        data = request.get_json()

        if isinstance(data,dict):
            if ("words" in data) and ("order" in data):
                if isinstance(data["words"],list) and (len(data["words"])>0):
                    for item in data["words"]:
                        if not isinstance(item,str):
                            return {"message":"The content of the list provided must be a string."}, 400

                    if isinstance(data['order'],str):
                        if data["order"] == "asc":
                            data['words'].sort(); return data['words'], 200
                        elif data["order"] == "desc":
                            data['words'].sort(reverse=True); return data['words'], 200
                        else:
                            return {"message":"The 'order' must be either 'asc' or 'desc'"}, 400
                    else:
                        return {"message":"The content of the item 'order' must be string, either 'desc' or 'asc'."}, 400 
 
        return {"message":"The content provided must be a dictionary with the keys 'words':list (non empty) and 'order':str. For for instance: {'words': ['batman', 'robin', 'coringa'], 'order': 'desc'}"}, 400

    def get(self):
        return {"message":"Route allows just POST method"}, 400
    
    def put(self):
        return {"message":"Route allows just POST method"}, 400

    def delete(self):
        return {"message":"Route allows just POST method"}, 400


api.add_resource(VowelCounter,"/vowel_count")
api.add_resource(Sorter,"/sort")

if __name__ == "__main__":

    app.run(port=5000,debug=True)

